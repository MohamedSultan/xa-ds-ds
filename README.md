Thanks to [SpringDeveloper](https://www.youtube.com/channel/UC7yfnfvEUlXUIfm8rGLwZdA) for his amazing demo.

What I did is to write it down to facilitate troubleshooting and understandings

Just clone and run.

To test, 

```
run curl -d'{"name": "Spring Fans"}' -H"content-type: application/json" http://localhost:9090
```
or 

```

run curl -d'{"name": "Spring Fans"}' -H"content-type: application/json" http://localhost:9090
```


To check what is inserted into DB
run 

```
http://localhost:9090/pets
http://localhost:9090/messages
```
